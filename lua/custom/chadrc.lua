-- First read our docs (completely) then check the example_config repo

local M = {}

M.ui = {
  theme = "everforest",
  theme_toggle = { "everforest", "one_light" },
}

return M
